from pony.orm import *


db = Database()
db.bind(provider='sqlite', filename='data.db', create_db=True)


class Category(db.Entity):
    id = PrimaryKey(int, auto=True)
    caption = Required(str)
    subcategories = Set('Category', reverse='parent')
    parent = Optional('Category', reverse='subcategories')
    types_record = Set('TypeRecord')


class Tag(db.Entity):
    id = PrimaryKey(int, auto=True)
    text = Optional(str)
    normal = Optional(str)
    types_record = Set('TypeRecord')


class TypeRecord(db.Entity):
    id = PrimaryKey(int, auto=True)
    caption = Optional(str)
    category = Required(Category)
    tags = Set(Tag)
    attributes = Set('Attribute')
    records = Set('Record')


class Attribute(db.Entity):
    id = PrimaryKey(int, auto=True)
    caption = Optional(str)
    type_record = Required(TypeRecord)
    type_attribute = Required('TypeAttribune')
    data_points = Set('DataPoint')


class TypeAttribune(db.Entity):
    id = PrimaryKey(int, auto=True)
    caption = Optional(str)
    attributes = Set(Attribute)


class Record(db.Entity):
    id = PrimaryKey(int, auto=True)
    type_record = Required(TypeRecord)
    data_points = Set('DataPoint')


class DataPoint(db.Entity):
    id = PrimaryKey(int, auto=True)
    record = Required(Record)
    attribute = Required(Attribute)
    value = Optional(str)


db.generate_mapping(create_tables=True)