from flask import (
    abort,
    Flask, 
    request, 
    render_template, 
    redirect,
    url_for,
)
from storage import *
from pony.orm import select, commit, db_session


app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/categories')
@db_session
def categories():
    print('c1')
    categories = select(c for c in Category).order_by(Category.caption)
    print('c2')
    return render_template('categories.html', categories=categories)


@app.route('/categories/new', methods=['POST', 'GET'])
@db_session
def new_category():
    if request.method == 'POST':
        print('nc1')
        categ = Category(caption = request.form['caption'])
        print('nc2')
        sel_value = request.form['parent']
        print('nc3')
        if sel_value != '0':
            categ.parent = select(c for c in Category if c.id == int(sel_value))
        commit()
        print('nc4')
        return redirect(url_for('categories'))
    else:
        categories = select(c for c in Category).order_by(Category.caption)
        return render_template('new_category.html', categories=categories)


